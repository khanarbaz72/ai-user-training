**INTRODUCTION**
- Our Brain has numerous number of neurons for perceiving the objects and making the decisions likewise we have artificial neurons in the neural network as a mathematical function conceived as a model of biological neurons.

**NEURAL NETWORK**
A neural network is a collection of neurons and can adapt to changing input so the network generates the best possible result without needing to redesign the output criteria.
![neural_network][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/neuron.png]

**NEURON**
A neuron is contained inside the neural network and the value ranges between 0 and 1 in them.Each neuron acts as a matheamtical operation that takes its input multiplies it by it's weights and then passes the sum through the activation function to the other neurons.
![neurons][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/neuron-1.png]

**ACTIVATION NUMBER**
-The number inside the neuron between the range 0 and 1 is called activation number.
-Activation in first layer correspond some activation in hidden layers to reach upto the final layer and predict the final 
output number.Each layer gets as an input from the prvious layers output and weighted sum is calculated by applying activation function and wieghts and final ouput is generated in the ouput layer.
![activation][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/activation.png]

**BIAS**
A bias is an additional parameter in the Neural Network which is used to adjust the output along with the weighted sum of the inputs to the neuron.It is a constant which helps the model in a way that it can fit best for the given data.
![bias][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/bias.png]

**SIGMOID FUNCTION**
A function that squishes the real number line into the range between 0 and 1 and it is also called as a logistic curve.
![sigmoid][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/sigmoid.png]
![sigmoid-1][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/sigmoid-1.png]

**ReLU**
It stands for Rectified Linear Unit which gives 0 for negative and acts as identity function for positive values.
Results in faster DNNs.
![ReLU][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/RELU.png]

**COST FUNCTION**
It is used to determine how bad is the network. Needs to be minimised for a better DNN.
![cost_function][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/cost_function.png]

**GRADIENT DESCENT**
- Gradient descent is a first-order iterative optimization algorithm for finding a local minimum of a differentiable function.
- Gradient of the value gives the direction of steepest ascent
- It is used to find one of the local minimas to minimise output of cost function.
![gradient_descent][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Gradient_Descent.png]

**BACK PROPOGATION**
- It is the essence of neural net training. 
- It is the method of fine-tuning the weights of a neural net based on the error rate obtained in the previous iteration.
- Proper tuning of the weights allows you to reduce error rates and to make the model reliable by increasing its generalization.
 ![back_propagation][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Backprop.png]

**STOCHASTIC GRADIENT DESCENT**
- It is a technique in which the large training data is divided into mini batches and gradient descent of those mini batches is found using backprop iteratively. 
- It is computationally faster than finding the gradient of whole training data at a time.
![STOCHASTIC_GRADIENT_DESCENT][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Stochastic_Gradient_Descent.png]
 