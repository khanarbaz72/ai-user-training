**CNN(CONVOLUTIONAL NEURAL NETWORK)**
- CNN is a class of Deep Neural Network(DNN), also known as ConvNet basically used for classification.
- Areas of Applications are Image recognition, Image classification, Object detection, Recognition face etc.
- Features of an image help us to know about it.
- Features are separated using filters .
![cnn][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/cnn-1.png]

1)Input : image into convolution layer

2) Choose parameters, apply filters(matrix) with strides, padding if requires. Perform convolution on the image and apply ReLU activation to the matrix.

**Important Terms**
- **Strides** : Stride is the number of pixels shifts over the input matrix.  ![stride][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Stride.png]
- **Kernels** : Concatenation of filters. ![kernel][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/kernel.png]
- **Padding** : If filter doesn't fit properly, padding takes place which are of two types.
 - **ZERO PADDING** :pad with enough zeros till it fits.  ![zero_padding][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Padding.png]
 - **VALID PADDING** :keeping only the valid part, & drop the remaining where filter doesn't fit. 
- **ReLU** :Introduces non-linearity(to learn non-negative liner values).
- **Convoluitonal Layer** : The first layer that extracts features from input image.
- **Pooling Layer** : Pooling layers section would reduce the number of parameters when the images are too large. ![pooling][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/Pooling.png]
- **Fully Connected Layer** : The feature maps are flattened to bring them into one single vector and that becomes Fully
Connected Layer.
- **Flattening** : Flattening layer is used to bring all feature maps matrices into a single vector.

**HOW CNN WORKS**
Convolutional neural network are used to find patterns in an image. You do that by convoluting over an image and looking for patterns. In the first few layers of CNNs the network can identify lines and corners, but we can then pass these patterns down through our neural net and start recognizing more complex features as we get deeper. This property makes CNNs really good at identifying objects in images.
![cnn_working][https://gitlab.com/khanarbaz72/ai-user-training/-/blob/master/Images/cnn_working.png]










